def vowel_swapper(string):
    # ==============
    message=string
    Vowels= ("a","A", "e","E","i","I","o","O","u","U")
    Replacement= ("4","4","3","3","!","!","ooo","OOO","¦_¦","¦___¦" )
    new_message= ""
    for letter in message:
        if letter not in Vowels:
            new_message += letter
        else:
            new_message += Replacement[Vowels.index(letter)]
    print(new_message)

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console

