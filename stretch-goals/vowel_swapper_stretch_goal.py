def vowel_swapper(string):
    # ==============
    # Your code here
    message=string
    Vowels = ("a", "A", "e", "E", "i", "I", "o", "O", "u", "U")
    Replacement = ("/\\", "/\\", "3", "3", "!", "!", "000","000", "\/", "\/")
    VowelCounter = {"a":0, "A":0, "e":0, "E":0, "i":0, "I":0, "o":0, "O":0, "u":0, "U":0}
    new_message = ""
    for letter in message:
        if letter not in Vowels:
            new_message += letter
        elif letter in Vowels and (VowelCounter[letter] == 1):
            new_message += Replacement[Vowels.index(letter)]
            VowelCounter[letter.lower()] = 0
            VowelCounter[letter.upper()] = 0
        elif letter in Vowels and (VowelCounter[letter] < 1):
            VowelCounter[letter.lower()] = VowelCounter[letter.lower()] + 1
            VowelCounter[letter.upper()] = VowelCounter[letter.upper()] + 1
            new_message += letter
    print(new_message)
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console

