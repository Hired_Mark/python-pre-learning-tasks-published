def factors(number):


# ==============
# Your code here
    printlist=[]
    for factor in range (2,number):
        if number%factor == 0:
            printlist.append(factor)

    if len(printlist) == 0:
        print(number, "is a prime number")
    else:
        print(printlist)
# ==============

print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
